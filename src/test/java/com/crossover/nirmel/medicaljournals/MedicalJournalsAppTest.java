/**
 * 
 */
package com.crossover.nirmel.medicaljournals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.crossover.nirmel.medicaljournals.components.CustomUserTokenConverter;
import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.crossover.nirmel.medicaljournals.entities.Permission;
import com.crossover.nirmel.medicaljournals.entities.Role;
import com.crossover.nirmel.medicaljournals.entities.User;
import com.crossover.nirmel.medicaljournals.models.SecurityUser;

/**
 * JUnit test cases
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class MedicalJournalsAppTest {

	@Test
	public void securityUserTest() throws Exception {		
		SecurityUser securityUser = getSecurityUser();
		
		Collection<? extends GrantedAuthority> authorities = securityUser.getAuthorities();
		
		assertEquals(authorities.size(), 3);
		
		Collection<String> expectedAuthorities = Arrays.asList(Authority.PERM_UPLOAD_JOURNAL, Authority.PERM_MANAGE_JOURNALS, Authority.ROLE_PUBLISHER);
		
		for (GrantedAuthority grantedAuthority : authorities) {
			assertTrue(expectedAuthorities.contains(grantedAuthority.getAuthority()));
		}
		
		assertTrue(securityUser.getEnabled());
	}
	
	private SecurityUser getSecurityUser() {
		Permission perm1 = new Permission(Authority.PERM_UPLOAD_JOURNAL);
		Permission perm2 = new Permission(Authority.PERM_MANAGE_JOURNALS);
		
		Role role = new Role(Authority.ROLE_PUBLISHER);
		role.getPermissions().add(perm1);
		role.getPermissions().add(perm2);
		
		User user = new User();
		user.setId(1L);
		user.setFirstName("John");
		user.setLastName("Doe");
		user.setEmail("john.doe@gmail.com");
		user.setEnabled(true);
		user.setPassword("secret");
		user.getUserRoles().add(role);
		
		SecurityUser securityUser = new SecurityUser(user);
		
		return securityUser;
	}
	
	@Test
	public void customUserTokenConverterTest() {
		SecurityUser securityUser = getSecurityUser();
		
		CustomUserTokenConverter converter = new CustomUserTokenConverter();
		
		InMemoryUserDetailsManager userDetailsService = new InMemoryUserDetailsManager(Arrays.asList(securityUser));
		
		converter.setUserDetailsService(userDetailsService);
		
		UserDetails userDetails = userDetailsService.loadUserByUsername("john.doe@gmail.com");
		
		assertNotNull(userDetails);
		
		assertEquals(userDetails.getAuthorities().size(), 3);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("user_id", securityUser.getId());
		map.put(UserAuthenticationConverter.AUTHORITIES, Arrays.asList(Authority.PERM_UPLOAD_JOURNAL, Authority.PERM_MANAGE_JOURNALS, Authority.ROLE_PUBLISHER));
		map.put(UserAuthenticationConverter.USERNAME, securityUser.getEmail());
		
		Authentication auth = converter.extractAuthentication(map);
		
		Map<String, ?> newMap = converter.convertUserAuthentication(auth);
		
		assertEquals(newMap.get("user_id"), securityUser.getId());
	}
}
