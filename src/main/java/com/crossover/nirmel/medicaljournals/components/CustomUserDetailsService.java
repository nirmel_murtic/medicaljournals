/**
 * 
 */
package com.crossover.nirmel.medicaljournals.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.crossover.nirmel.medicaljournals.entities.User;
import com.crossover.nirmel.medicaljournals.models.SecurityUser;
import com.crossover.nirmel.medicaljournals.repositories.UserRepository;

/**
 * Custom detail service used by spring security
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@Component
public class CustomUserDetailsService implements UserDetailsService {
	
    @Autowired
    private UserRepository userRepository;
     
    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
    	
        User user = userRepository.findByEmail(email);
        
        if(user == null){
            throw new UsernameNotFoundException("User email " + email + " is not found");
        }
        
        if(!user.getEnabled()){
            throw new UsernameNotFoundException("User is disabled");
        }
        
        return new SecurityUser(user);
    }
}