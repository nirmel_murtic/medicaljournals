/**
 * 
 */
package com.crossover.nirmel.medicaljournals.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.crossover.nirmel.medicaljournals.controllers.BaseController;
import com.crossover.nirmel.medicaljournals.repositories.UserRepository;

/**
 * Security auditor, used by auditing service
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@Component
public class SecurityAuditor implements AuditorAware<Long> {
	
	@Autowired
	protected UserRepository userRepository;
	
	public SecurityAuditor() {
	}
	
    @Override
    public Long getCurrentAuditor() {
        return BaseController.getCurrentUserId();
    }
}
