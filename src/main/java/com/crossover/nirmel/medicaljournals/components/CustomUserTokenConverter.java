/**
 * 
 */
package com.crossover.nirmel.medicaljournals.components;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import com.crossover.nirmel.medicaljournals.models.SecurityUser;

/**
 * Custom user authentication converter
 * 
 * Used for adding additional informations to jwt token (e.g. user_id)
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public class CustomUserTokenConverter extends DefaultUserAuthenticationConverter {

		@SuppressWarnings("unchecked")
		@Override
		public Map<String, ?> convertUserAuthentication(Authentication authentication) {
			Map<String, Object> result =  (Map<String, Object>) super.convertUserAuthentication(authentication);
			
			Long userId = null;
			
			if(authentication.getPrincipal() instanceof SecurityUser) {
				userId = ((SecurityUser) authentication.getPrincipal()).getId();	
			} else if(authentication.getDetails() != null) {
				if(authentication.getDetails() instanceof Map) {
					Map<String, Object> details = (Map<String, Object>) authentication.getDetails();
					
					userId = (Long) details.get("user_id");
				}
			}
			
			if(userId != null) {
				result.put("user_id", userId);	
			}
			
			return result;
		}
		
		@Override
		public Authentication extractAuthentication(Map<String, ?> map) {
			Authentication auth = super.extractAuthentication(map);
			
			if(auth instanceof UsernamePasswordAuthenticationToken) {
				Map<String, Object> details = new HashMap<String, Object>();
				
				if(map.containsKey("user_id")) {
					if(map.get("user_id") instanceof Long) {
						details.put("user_id", map.get("user_id"));
					} else if(map.get("user_id") instanceof Integer) {
						Integer userId = (Integer) map.get("user_id");
						
						details.put("user_id", userId.longValue());
					}
				}
				
				((UsernamePasswordAuthenticationToken) auth).setDetails(details);
			}
			
			return auth;
		}
	}

