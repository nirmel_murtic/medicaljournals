/**
 * 
 */
package com.crossover.nirmel.medicaljournals.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.crossover.nirmel.medicaljournals.entities.Subscription;

/**
 * Journal release data repository
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public interface JournalSubscriptionRepository extends CrudRepository<Subscription, Long> {
	/**
	 * Get subscription by user and journal
	 * 
	 * @param userId
	 * @param journalId
	 * @return
	 * @since 1.0
	 */
	@Query("select s from Subscription s where s.user.id = ?1 and s.journal.id = ?2 and s.removed = 0")
	Subscription findSubscription(Long userId, Long journalId);
}