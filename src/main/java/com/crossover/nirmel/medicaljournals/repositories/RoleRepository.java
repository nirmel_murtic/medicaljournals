/**
 * 
 */
package com.crossover.nirmel.medicaljournals.repositories;

import org.springframework.data.repository.CrudRepository;

import com.crossover.nirmel.medicaljournals.entities.Role;

/**
 * Role data repository
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		Apr 30, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
	
	/**
	 * Load role by name
	 * 
	 * @param name
	 * @return
	 * @since 1.0
	 */
	public Role findByName(String name);
}