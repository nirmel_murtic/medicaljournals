/**
 * 
 */
package com.crossover.nirmel.medicaljournals.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.crossover.nirmel.medicaljournals.entities.Journal;

/**
 * Journal data repository
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public interface JournalRepository extends CrudRepository<Journal, Long> {
	/**
	 * Filter user journals by name
	 * 
	 * @param userId
	 * @param name
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j where j.createdById = ?1 and (j.name like %?2% or j.description like %?2%) and j.removed = 0")
	Page<Journal> filterByNamePaged(Long userId, String name, Pageable pageable);
	
	/**
	 * Load journals by owner
	 * 
	 * @param userId
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j where j.createdById = ?1 and j.removed = 0")
	Page<Journal> findByUserIdPaged(Long userId, Pageable pageable);
	
	/**
	 * Filter all journals filtered by name
	 * 
	 * @param name
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j where (j.name like %?1% or j.description like %?1%) and j.removed = 0")
	Page<Journal> filterByNamePaged(String name, Pageable pageable);
	
	/**
	 * Load all journals
	 * 
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j where j.removed = 0")
	Page<Journal> findAllPaged(Pageable pageable);
	
	/**
	 * Load subscribed journals
	 * 
	 * @param userId
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j join j.subscriptions s where s.user.id = ?1 and (j.name like %?2% or j.description like %?2%) and j.removed = 0 and s.removed = 0")
	Page<Journal> findSubscribedJournals(Long userId, String name, Pageable pageable);
	
	/**
	 * Load unsubscribed journals
	 * 
	 * @param userId
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select j from Journal j where (j.name like %?2% or j.description like %?2%) and j.removed = 0 and j.id not in (select j from Journal j join j.subscriptions s where s.user.id = ?1 and s.removed = 0)")
	Page<Journal> findUnsubscribedJournals(Long userId, String name, Pageable pageable);
}