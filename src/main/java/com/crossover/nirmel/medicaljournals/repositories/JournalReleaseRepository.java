/**
 * 
 */
package com.crossover.nirmel.medicaljournals.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.crossover.nirmel.medicaljournals.entities.JournalRelease;

/**
 * Journal release data repository
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public interface JournalReleaseRepository extends CrudRepository<JournalRelease, Long> {
	/**
	 * Load journals by owner
	 * 
	 * @param journalId
	 * @param userId
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select r from JournalRelease r where r.journal.id = ?1 and r.removed = 0")
	Page<JournalRelease> findByJournalIdPaged(Long journalId, Pageable pageable);
}