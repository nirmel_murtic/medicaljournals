/**
 * 
 */
package com.crossover.nirmel.medicaljournals.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.crossover.nirmel.medicaljournals.entities.User;

/**
 * User data repository
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		Apr 30, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
	/**
	 * Load user by email
	 * 
	 * @param email
	 * @return
	 * @since 1.0
	 */
	public User findByEmail(String email);
	
	/**
	 * 
	 * @param name
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	@Query("select u from User u where u.email like %?1% or u.firstName like %?1% or u.lastName like %?1% ")
	Page<User> filterByNamePaged(String name, Pageable pageable);
	
	/**
	 * 
	 * @param pageable
	 * @return
	 * @since 1.0
	 */
	Page<User> findAll(Pageable pageable);
}