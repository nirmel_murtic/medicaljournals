package com.crossover.nirmel.medicaljournals;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Application, entry point
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@SpringBootApplication
@EnableJpaAuditing
@Configuration
public class Application extends SpringBootServletInitializer {
	
	protected Logger logger = Logger.getLogger(Application.class.getName());
	
	public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
