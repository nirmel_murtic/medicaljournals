package com.crossover.nirmel.medicaljournals.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.crossover.nirmel.medicaljournals.entities.JournalRelease;
import com.crossover.nirmel.medicaljournals.entities.Subscription;
import com.crossover.nirmel.medicaljournals.repositories.JournalReleaseRepository;
import com.crossover.nirmel.medicaljournals.repositories.JournalRepository;
import com.crossover.nirmel.medicaljournals.repositories.JournalSubscriptionRepository;

/**
 * REST API for journal resources
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@RestController
@RequestMapping("/api/releases")
public class JournalReleasesController extends BaseController {
	
	@Value("${fileLocation}")
    private String fileLocation;

	@Autowired
	private JournalRepository journalRepository;
	
	@Autowired
	private JournalReleaseRepository journalReleaseRepository;
	
	@Autowired
	private JournalSubscriptionRepository journalSubscriptionRepository;
	/**
	 * Load journal releases
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/{journalReleaseId}")
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<JournalRelease> getJournalRelease(@PathVariable Long journalReleaseId) { 

		JournalRelease release = journalReleaseRepository.findOne(journalReleaseId);
		
		if(release == null || release.getRemoved()) {
			throw new NoResultException("Journal release doesn't exist.");
		}
		
		// In case release is created by current user, return release immediately without checking subscription
		if(release.getCreatedById() != null && release.getCreatedById().equals(getCurrentUserId())) {
			return new ResponseEntity<JournalRelease>(release, HttpStatus.OK);
		}
		
		Subscription subscription = journalSubscriptionRepository.findSubscription(getCurrentUserId(), release.getJournal().getId());
		
		if(subscription != null) {
			return new ResponseEntity<JournalRelease>(release, HttpStatus.OK);
		} else {
			throw new AccessDeniedException("You are not subscribed to provided journal.");
		}
    }
	
	/**
	 * Delete journal resource by id
	 * 
	 * @param id
	 * @return
	 * @since 1.0
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAuthority('" + Authority.PERM_MANAGE_JOURNALS + "')")
    public ResponseEntity<Void> deleteJournalRelease(@PathVariable Long id) { 		
		JournalRelease release = journalReleaseRepository.findOne(id);
		
		if(release == null || release.getRemoved()) {
			throw new NoResultException("Journal release doesn't exist.");
		}
		
		release.setRemoved(true);
		
		journalReleaseRepository.save(release);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	/**
	 * Create journal release
	 * 
	 * @param journal
	 * @return
	 * @throws Exception 
	 * @since 1.0
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('" + Authority.PERM_MANAGE_JOURNALS + "')")
    public ResponseEntity<JournalRelease> createJournalRelease(@RequestParam String releaseCode,
    		@RequestParam Long journalId,
    		@RequestParam("file") MultipartFile file) throws Exception { 		
		
		JournalRelease journalRelease = new JournalRelease();
		journalRelease.setCode(releaseCode);
		journalRelease.setJournal(journalRepository.findOne(journalId));
		
		if (!file.isEmpty()) {
			try {
				File f = new File(fileLocation + "/" + file.getOriginalFilename());
				
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(f));
				
                FileCopyUtils.copy(file.getInputStream(), stream);
                
				stream.close();
				
				journalRelease.setLocation(file.getOriginalFilename());
				journalRelease.setType(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")).replace(".", ""));
			}
			catch (Exception e) {
				throw new Exception("You failed to upload " + journalRelease.getLocation());
			}
		}
		
		journalRelease = journalReleaseRepository.save(journalRelease);	
        
        return new ResponseEntity<JournalRelease>(journalRelease, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{journalReleaseId}/source", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
	public void getImage(@PathVariable Long journalReleaseId, HttpServletResponse response) throws Exception {
		
		JournalRelease release = journalReleaseRepository.findOne(journalReleaseId);
		
		if(release == null || release.getRemoved()) {
			throw new NoResultException("Journal release doesn't exist.");
		}
		
		Subscription subscription = journalSubscriptionRepository.findSubscription(getCurrentUserId(), release.getJournal().getId());
		
		if(subscription != null 
				|| ((release.getCreatedById() != null && release.getCreatedById().equals(getCurrentUserId())) 
						|| hasAuthority(Authority.PERM_MANAGE_USERS))) {
			InputStream inputStream = new FileInputStream(fileLocation + "/" + release.getLocation());
			
			BufferedOutputStream stream = new BufferedOutputStream(
					response.getOutputStream());
			
            response.setContentType("application/" + release.getType());
			
			response.setHeader("Cache-Control", "max-age=86400");
			
            FileCopyUtils.copy(inputStream, stream);
		} else {
			throw new AccessDeniedException("You are not subscribed to provided journal.");
		}
	}
}
