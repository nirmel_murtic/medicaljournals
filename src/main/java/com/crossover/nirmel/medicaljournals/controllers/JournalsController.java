package com.crossover.nirmel.medicaljournals.controllers;

import javax.persistence.NoResultException;
import javax.xml.bind.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.crossover.nirmel.medicaljournals.entities.Journal;
import com.crossover.nirmel.medicaljournals.entities.JournalRelease;
import com.crossover.nirmel.medicaljournals.entities.Subscription;
import com.crossover.nirmel.medicaljournals.repositories.JournalReleaseRepository;
import com.crossover.nirmel.medicaljournals.repositories.JournalRepository;
import com.crossover.nirmel.medicaljournals.repositories.JournalSubscriptionRepository;

/**
 * REST API for journals
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@RestController
@RequestMapping("/api/journals")
public class JournalsController extends BaseController {
	
	@Autowired
	private JournalRepository journalRepository;
	
	@Autowired
	private JournalReleaseRepository journalReleaseRepository;
	
	@Autowired
	private JournalSubscriptionRepository journalSubscriptionRepository;
	
	/**
	 * Load all journals, pagination and filtering by name included
	 * 
	 * If userId is provided only owned journals will be returned
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("")
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<Iterable<Journal>> getJournals(Pageable pageable, @RequestParam(required = false) String name, @RequestParam(required = false) Long userId) { 
		Iterable<Journal> journals = null;
		
		if(userId != null) {
			if(name != null && !name.isEmpty()) {
				journals = journalRepository.filterByNamePaged(userId, name, pageable);
			} else {
				journals = journalRepository.findByUserIdPaged(userId, pageable);
			}
		} else {
			if(name != null && !name.isEmpty()) {
				journals = journalRepository.filterByNamePaged(name, pageable);
			} else {
				journals = journalRepository.findAllPaged(pageable);
			}
		}
		
		return new ResponseEntity<Iterable<Journal>>(journals, HttpStatus.OK);
    }
	
	/**
	 * Load journal by id
	 * 
	 * @param id
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/{id}")
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<Journal> getJournal(@PathVariable Long id) { 
		Journal journal = journalRepository.findOne(id);
		
		if(journal == null || journal.getRemoved()) {
			throw new NoResultException("Journal doesn't exist.");
		}
		
		return new ResponseEntity<Journal>(journal, HttpStatus.OK);
    }
	
	/**
	 * Create or update journal
	 * 
	 * @param journal
	 * @return
	 * @throws ValidationException 
	 * @since 1.0
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('" + Authority.PERM_MANAGE_JOURNALS + "')")
    public ResponseEntity<Object> createOrUpdateJournal(@RequestBody Journal journal) throws ValidationException { 		
		if(journal.getId() != null) {
			Journal existingJournal = journalRepository.findOne(journal.getId());
			
			if(existingJournal == null || existingJournal.getRemoved()) {
				throw new NoResultException("Journal doesn't exist.");
			}
			
			existingJournal.setName(journal.getName());
			existingJournal.setDescription(journal.getDescription());
			
			journal = journalRepository.save(existingJournal);	
		} else {
	        journal = journalRepository.save(journal);	
		}
        
        return new ResponseEntity<Object>(journal, HttpStatus.CREATED);
    }
	
	/**
	 * Delete journal by id
	 * 
	 * @param id
	 * @return
	 * @since 1.0
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasAuthority('" + Authority.PERM_MANAGE_JOURNALS + "')")
    public ResponseEntity<Void> deleteJournal(@PathVariable Long id) { 		
		Journal journal = journalRepository.findOne(id);
		
		if(journal == null || journal.getRemoved()) {
			throw new NoResultException("Journal doesn't exist.");
		}
		
		journal.setRemoved(true);
		
		journalRepository.save(journal);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	/**
	 * Load journal releases
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/{journalId}/releases")
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<Iterable<JournalRelease>> getJournalReleases(@PathVariable Long journalId, Pageable pageable) { 
		Subscription subscription = journalSubscriptionRepository.findSubscription(getCurrentUserId(), journalId);
		
		if(subscription == null) {
			Journal journal = journalRepository.findOne(journalId);
			
			if(journal != null && (journal.getCreatedById() != null && journal.getCreatedById().equals(getCurrentUserId()) || hasAuthority(Authority.PERM_MANAGE_USERS))) {
				Iterable<JournalRelease> releases = journalReleaseRepository.findByJournalIdPaged(journalId, pageable);
				
				return new ResponseEntity<Iterable<JournalRelease>>(releases, HttpStatus.OK);
			}
		}
		
		if(subscription != null) {
			Iterable<JournalRelease> releases = journalReleaseRepository.findByJournalIdPaged(journalId, pageable);
			
			return new ResponseEntity<Iterable<JournalRelease>>(releases, HttpStatus.OK);
		} else {
			throw new AccessDeniedException("You are not subscribed to provided journal.");
		}
    }
	
	/**
	 * Load subscribed journals
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/subscribedJournals")
	@PreAuthorize("hasAnyAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "', '" + Authority.PERM_MANAGE_USERS + "')")
    public ResponseEntity<Iterable<Journal>> getSubscribedJournals(Pageable pageable, @RequestParam(required = false, defaultValue = "") String name, @RequestParam(required = false) Long userId) {
		if(userId == null || !hasAuthority(Authority.PERM_MANAGE_USERS)) {
			userId = getCurrentUserId();
		}
		
		Iterable<Journal> journals = journalRepository.findSubscribedJournals(userId, name, pageable);
		
		return new ResponseEntity<Iterable<Journal>>(journals, HttpStatus.OK);
    }
	
	/**
	 * Load unsubscribed journals
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/unsubscribedJournals")
	@PreAuthorize("hasAnyAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "', '" + Authority.PERM_MANAGE_USERS + "')")
    public ResponseEntity<Iterable<Journal>> getUnsubscribedJournals(Pageable pageable, @RequestParam(required = false, defaultValue = "") String name, @RequestParam(required = false) Long userId) {
		if(userId == null || !hasAuthority(Authority.PERM_MANAGE_USERS)) {
			userId = getCurrentUserId();
		}
		
		Iterable<Journal> journals = journalRepository.findUnsubscribedJournals(userId, name, pageable);
		
		return new ResponseEntity<Iterable<Journal>>(journals, HttpStatus.OK);
    }
	
	/**
	 * Subscribe to journal
	 * 
	 * @param journal
	 * @return
	 * @throws ValidationException 
	 * @since 1.0
	 */
	@RequestMapping(value = "/{journalId}/subscribe", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<Subscription> subscribeToJournal(@PathVariable Long journalId) throws ValidationException { 		
		Journal existingJournal = journalRepository.findOne(journalId);
		
		if(existingJournal == null || existingJournal.getRemoved()) {
			throw new NoResultException("Journal doesn't exist.");
		}
		
		Subscription subscription = journalSubscriptionRepository.findSubscription(getCurrentUserId(), journalId);
		
		if(subscription != null) {
			throw new ValidationException("You're already subscribed to provided journal");
		} else {
			subscription = new Subscription();
			subscription.setUser(getCurrentUser());
			subscription.setJournal(existingJournal);
			
			journalSubscriptionRepository.save(subscription);
		}
        
        return new ResponseEntity<Subscription>(subscription, HttpStatus.CREATED);
    }
	
	/**
	 * Unsubscribe from journal
	 * 
	 * @param journal
	 * @return
	 * @throws ValidationException 
	 * @since 1.0
	 */
	@RequestMapping(value = "/{journalId}/unsubscribe", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('" + Authority.PERM_SUBSCRIBE_JOURNALS + "')")
    public ResponseEntity<Subscription> unsubscribeFromJournal(@PathVariable Long journalId) throws ValidationException { 		
		Journal existingJournal = journalRepository.findOne(journalId);
		
		if(existingJournal == null || existingJournal.getRemoved()) {
			throw new NoResultException("Journal doesn't exist.");
		}
		
		Subscription subscription = journalSubscriptionRepository.findSubscription(getCurrentUserId(), journalId);
		
		if(subscription != null) {
			subscription.setRemoved(true);
			
			subscription = journalSubscriptionRepository.save(subscription);
		} else {
			throw new ValidationException("You're not subscribed to provided journal");
		}
        
        return new ResponseEntity<Subscription>(subscription, HttpStatus.CREATED);
    }
}
