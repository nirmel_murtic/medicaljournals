/**
 * 
 */
package com.crossover.nirmel.medicaljournals.controllers;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.crossover.nirmel.medicaljournals.entities.BaseEntity;
import com.crossover.nirmel.medicaljournals.entities.User;
import com.crossover.nirmel.medicaljournals.models.ErrorMessage;
import com.crossover.nirmel.medicaljournals.repositories.UserRepository;

/**
 * Base class for all controllers
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public abstract class BaseController {

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	protected UserRepository userRepository;
	
	/**
	 * Handle no resource found error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoResultException.class)
	public ErrorMessage noResult(NoResultException e) {
		return new ErrorMessage(e, HttpStatus.NOT_FOUND, request);
	}
	
	/**
	 * Handle data integrity violation error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.CONFLICT)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ErrorMessage conflict(DataIntegrityViolationException e) {
		return new ErrorMessage(e, HttpStatus.CONFLICT, request);
	}
	
	/**
	 * Handle validation error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ValidationException.class)
	public ErrorMessage validation(ValidationException e) {
		return new ErrorMessage(e, HttpStatus.BAD_REQUEST, request);
	}
	
	/**
	 * Handle invalid operation error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(UnsupportedOperationException.class)
	public ErrorMessage unsupportedOperation(UnsupportedOperationException e) {
		return new ErrorMessage(e, HttpStatus.BAD_REQUEST, request);
	}
	
	/**
	 * Handle internal server error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public ErrorMessage internalServerError(Throwable e) {
		int counter = 0;
		
		while(e.getCause() != null || counter >= 10) {
			e = e.getCause();
			
			counter++;
		}
		
		return new ErrorMessage(e, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	/**
	 * Handle access denied error
	 * 
	 * @param e
	 * @return
	 * @since 1.0
	 */
	@ResponseStatus(code = HttpStatus.FORBIDDEN)
	@ExceptionHandler(AccessDeniedException.class)
	public ErrorMessage internalServerError(AccessDeniedException e) {
		return new ErrorMessage(e, HttpStatus.FORBIDDEN, request);
	}
	
	/**
	 * Get email for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	public static String getCurrentUserEmail() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		
		if(auth != null && auth.getPrincipal() != null) {
			Object principal = auth.getPrincipal();
			
			if(principal instanceof String) {
				return principal.toString();
			}
		}
		
		return null;
	}
	
	/**
	 * Get id for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	@SuppressWarnings("unchecked")
	public static Long getCurrentUserId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null && auth instanceof OAuth2Authentication && ((OAuth2Authentication)auth).getDetails() != null) {
			Authentication authentication = ((OAuth2Authentication)auth).getUserAuthentication();
			
			Map<String, Object> details = (Map<String, Object>)authentication.getDetails();
					
			if(details != null) {
				return (Long) details.get("user_id");
			}
		}
		
		return null;
	}
	
	/**
	 * Get client id for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	public static String getClientId() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null && auth instanceof OAuth2Authentication) {
			return ((OAuth2Authentication)auth).getOAuth2Request().getClientId();
		}
		
		return null;
	}
	
	/**
	 * Get authorities for logged in user
	 * 
	 * @return
	 */
	public static Set<String> getAuthorities() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Set<String> authorities = new HashSet<>();
		
		if(auth != null && auth.getAuthorities() != null) {
			for(GrantedAuthority authority : auth.getAuthorities()) {
				authorities.add(authority.getAuthority());
			}
		}
		
		return authorities;
	}
	
	/**
	 * Method will check if currently logged user has provided authority
	 * 
	 * @param authority
	 * @return
	 */
	public static boolean hasAuthority(String authority) {
		Set<String> availableAuthorities = getAuthorities();
		
		return availableAuthorities.contains(authority);
	}
	
	/**
	 * Method will check if currently logged user has any of provided authorities
	 * 
	 * @param authorities
	 * @return
	 */
	public static boolean hasAnyAuthority(String[] authorities) {
		Set<String> availableAuthorities = getAuthorities();
		
		for (String authority : authorities) {
			if(availableAuthorities.contains(authority)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Return user info for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	public User getCurrentUser() {
		String email = getCurrentUserEmail();
		
		User user = null;
		
		if(email != null) {
			user = userRepository.findByEmail(email);
		}
		
		return user;
	}
	
	/**
	 * Check if currently logged user has access to provided entity.
	 * Check if entity exist.
	 * 
	 * @param entity
	 * @since 1.0
	 */
	public void checkAccess(BaseEntity entity) throws AccessDeniedException, NoResultException {
		String currentUserEmail = getCurrentUserEmail();
		
		if(entity == null) {
			throw new NoResultException("Resource doesn't exist.");
		}
		
		if(entity.getCreatedBy() == null || !entity.getCreatedBy().getEmail().equals(currentUserEmail)) {
			throw new AccessDeniedException("You don't have permission to access this resource.");
		}
	}
}
