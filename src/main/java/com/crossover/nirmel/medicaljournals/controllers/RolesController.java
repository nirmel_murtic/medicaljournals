package com.crossover.nirmel.medicaljournals.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.crossover.nirmel.medicaljournals.entities.Role;
import com.crossover.nirmel.medicaljournals.repositories.RoleRepository;

/**
 * REST API for roles
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@RestController
@RequestMapping("/api/roles")
public class RolesController extends BaseController {
	
	@Autowired
	protected RoleRepository roleRepository;
	
	/**
	 * Get all roles
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("")
	@PreAuthorize("hasRole('" + Authority.ROLE_ADMIN + "')")
    public ResponseEntity<Iterable<Role>> getRoles() { 
		Iterable<Role> roles = roleRepository.findAll();
		
		return new ResponseEntity<Iterable<Role>>(roles, HttpStatus.OK);
    }
}
