package com.crossover.nirmel.medicaljournals.controllers;

import javax.persistence.NoResultException;

import org.hibernate.metamodel.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.crossover.nirmel.medicaljournals.entities.Role;
import com.crossover.nirmel.medicaljournals.entities.User;
import com.crossover.nirmel.medicaljournals.repositories.RoleRepository;

/**
 * REST API for users
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@RestController
@RequestMapping("/api/users")
public class UserController extends BaseController {
	
	@Autowired
	protected RoleRepository roleRepository;
	
	/**
	 * Get user informations for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/currentUser")
	@PreAuthorize("isAuthenticated()")
    public ResponseEntity<User> getUser() {
		User user = getCurrentUser();
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
    }
	
	/**
	 * Get user informations for currently logged user
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@PreAuthorize("permitAll()")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
		BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
		
		user.setPassword(bcryptEncoder.encode(user.getPassword()));

		user.getUserRoles().add(roleRepository.findByName(Authority.ROLE_USER.toString()));
		
		user = userRepository.save(user);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
    }
	
	/**
	 * Load all users, pagination and filtering by name included
	 * 
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("")
	@PreAuthorize("hasAnyAuthority('" + Authority.PERM_MANAGE_USERS + "')")
    public ResponseEntity<Iterable<User>> getAllUsers(Pageable pageable, String search) { 
		Iterable<User> users = null;
		
		if(search != null && !search.isEmpty()) {
			users = userRepository.filterByNamePaged(search, pageable);
		} else {
			users = userRepository.findAll(pageable);
		}
		
		return new ResponseEntity<Iterable<User>>(users, HttpStatus.OK);
    }
	
	/**
	 * Load user by id
	 * 
	 * @param id
	 * @return
	 * @since 1.0
	 */
	@RequestMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('" + Authority.PERM_MANAGE_USERS + "')")
    public ResponseEntity<User> getUser(@PathVariable Long id) { 
		User user = userRepository.findOne(id);
		
		if(user == null) {
			throw new NoResultException("User doesn't exist.");
		}
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
    }
	
	/**
	 * Update user including status and user roles
	 * 
	 * @param user
	 * @return
	 * @since 1.0
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('" + Authority.PERM_MANAGE_USERS + "')")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
		if(user.getId() == null) {
			throw new ValidationException("User id needs to be provided");
		}
		
		User existingUser = userRepository.findOne(user.getId());
		
		if(existingUser == null) {
			throw new NoResultException("User doesn't exist.");
		}
		
		existingUser.setEmail(user.getEmail());
		existingUser.setEnabled(user.getEnabled());
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		
		// Check if currently logged user is being deactivated
		if(getCurrentUserId().equals(user.getId()) && !user.getEnabled()) {
			throw new ValidationException("Cannot deactivate currently logged user.");
		}
		
		// Check if manager is trying to update administrator user
		if(!hasAuthority(Authority.ROLE_ADMIN) && existingUser.isAdmin()) {
			throw new ValidationException("Cannot update user with higher permission level.");
		}
		
		// Update user roles
		if(hasAuthority(Authority.ROLE_ADMIN)) {
			existingUser.getUserRoles().clear();
			
			boolean hasUserRole = false;
			
			for(Role role : user.getUserRoles()) {
				if(role.getName().equals(Authority.ROLE_USER)) {
					hasUserRole = true;
				}
				
				existingUser.getUserRoles().add(roleRepository.findByName(role.getName()));
			}
			
			// Prevent removing user role
			if(!hasUserRole) {
				existingUser.getUserRoles().add(roleRepository.findByName(Authority.ROLE_USER));
			}
		}
		
		user = userRepository.save(existingUser);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
    }
	
	/**
	 * Update user including status and user roles
	 * 
	 * @param user
	 * @return
	 * @since 1.0
	 */
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
    public ResponseEntity<User> updateProfile(@RequestBody User user, String currentPassword) {
		if(user.getId() == null) {
			throw new ValidationException("User id needs to be provided");
		}
		
		BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
		
		User existingUser = userRepository.findOne(getCurrentUserId());
		
		existingUser.setEmail(user.getEmail());
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		
		if(user.getPassword() != null) {
			if(currentPassword == null || !bcryptEncoder.matches(currentPassword, existingUser.getPassword())) {
				throw new ValidationException("Wrong current password");
			}
			
			existingUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		}
		
		user = userRepository.save(existingUser);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}
