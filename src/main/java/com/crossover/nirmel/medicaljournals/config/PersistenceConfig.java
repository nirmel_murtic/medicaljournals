/**
 * 
 */
package com.crossover.nirmel.medicaljournals.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * Persistence configuration
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@Configuration
@EnableTransactionManagement
@EntityScan("com.crossover.nirmel.medicaljournals.entities")
@EnableJpaRepositories("com.crossover.nirmel.medicaljournals.repositories")
public class PersistenceConfig {
	
	/**
	 * Pooled data source is used, provided by c3p0 library
	 * 
	 * @return
	 * @since 1.0
	 */
	@Primary
    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource dataSource() {
		return new ComboPooledDataSource();
    }
}
