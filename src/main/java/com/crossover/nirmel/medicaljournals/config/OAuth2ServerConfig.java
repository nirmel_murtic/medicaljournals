package com.crossover.nirmel.medicaljournals.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.crossover.nirmel.medicaljournals.components.CustomUserTokenConverter;
import com.crossover.nirmel.medicaljournals.entities.User;

/**
 * OAuth 2 configuration
 * 
 * JWT tokens are used for communication
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@Configuration
public class OAuth2ServerConfig {

	@EnableResourceServer
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	    @Override
	    public void configure(HttpSecurity http) throws Exception {
	        http.authorizeRequests()
	        		.antMatchers("/api/users/register").permitAll()
	                .antMatchers("/api/**").authenticated()                     
	                .and().csrf()
	                .disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	        
	        http.headers().frameOptions().disable();
	    }

	}  
	
    @Configuration
    @EnableAuthorizationServer
    static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        private AuthenticationManager authenticationManager;

        @Autowired
        private ClientDetailsService clientDetailsService;
        
        @Autowired
        private DataSource dataSource;
        
        @Value("${spring.signingKey}")
        private String signingKey;

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        	clients.jdbc(dataSource);
        }
        
        @Override 
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception { 
            oauthServer.checkTokenAccess("permitAll()"); 
        }

        @Bean
        public JwtTokenStore tokenStore() {
            JwtTokenStore store = new JwtTokenStore(tokenEnhancer());
            
            store.setApprovalStore(approvalStore());
            
            return store;
        }
        
        @Bean 
        public ApprovalStore approvalStore() {
        	return new JdbcApprovalStore(dataSource);
        }

        @Bean
        public JwtAccessTokenConverter tokenEnhancer() {
            final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
            jwtAccessTokenConverter.setSigningKey(signingKey);
            jwtAccessTokenConverter.setAccessTokenConverter(jwtAccessTokenConverter());
            return jwtAccessTokenConverter;
        }
        
        @Bean
        public CustomUserTokenConverter jwtUserTokenConverter() {
        	return new CustomUserTokenConverter();
        }
        
        @Bean 
        public DefaultAccessTokenConverter jwtAccessTokenConverter() {
        	DefaultAccessTokenConverter converter = new DefaultAccessTokenConverter();
        	converter.setUserTokenConverter(jwtUserTokenConverter());
        	
        	return converter;
        }

        @Bean
        public TokenEnhancerChain tokenEnhancerChain() {
            final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
            tokenEnhancerChain.setTokenEnhancers(Arrays.asList(new MyTokenEnhancer(), tokenEnhancer()));
            return tokenEnhancerChain;
        }

        @Bean
        public AuthorizationServerTokenServices defaultAuthorizationServerTokenServices() {
        	final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
            defaultTokenServices.setTokenStore(tokenStore());
            defaultTokenServices.setClientDetailsService(clientDetailsService);
            defaultTokenServices.setTokenEnhancer(tokenEnhancerChain());
            defaultTokenServices.setSupportRefreshToken(true);
            
            return defaultTokenServices;
        }
        
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        	endpoints.tokenServices(defaultAuthorizationServerTokenServices()).authenticationManager(authenticationManager);
        }
        
        /**
         * Used for adding additional informations to jwt token
         * 
         * @author Nirmel
         *
         */
        private static class MyTokenEnhancer implements TokenEnhancer {
            @Override
            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
                if(authentication.getPrincipal() instanceof User) {
	            	final User user = (User) authentication.getPrincipal();
	                final Map<String, Object> additionalInfo = new HashMap<>();
	                additionalInfo.put("user_id", user.getId());
	                
	                ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
                }
                
                return accessToken;
            }
        }
    }
}