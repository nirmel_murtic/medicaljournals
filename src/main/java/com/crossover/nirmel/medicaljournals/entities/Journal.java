/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Journal data model
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@Entity
@Table(name = "journals")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Journal extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", columnDefinition = "text")
	private String description;
	
	@Column(name = "removed", nullable = false)
	private Boolean removed = false;
	
	@OneToMany(mappedBy="journal", fetch=FetchType.LAZY, cascade={CascadeType.PERSIST})
	@Where(clause="removed = 0")
	@JsonIgnore
	private Set<JournalRelease> releases = new HashSet<>();
	
	@OneToMany(mappedBy="journal", fetch=FetchType.LAZY, cascade={CascadeType.PERSIST})
	@Where(clause="removed = 0")
	@JsonIgnore
	private Set<Subscription> subscriptions = new HashSet<>();
	
	public Journal() {
	}

	public Journal(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/**
	 * Journal name
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Journal id
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Journal status (active/removed)
	 * 
	 * @return
	 * @since 1.0
	 */
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	/**
	 * Journal description
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Journal releases
	 * 
	 * @return
	 * @since 1.0
	 */
	public Set<JournalRelease> getReleases() {
		return releases;
	}

	public void setReleases(Set<JournalRelease> releases) {
		this.releases = releases;
	}
	
	/**
	 * Get journal subscriptions
	 * 
	 * @return
	 * @since 1.0
	 */
	public Set<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(Set<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	/**
	 * Get journal releases count
	 * 
	 * @return
	 * @since 1.0
	 */
	@Transient
	public Integer getJournalsCount() {
		return releases.size();
	}

	/**
	 * Get journal subscriptions count
	 * 
	 * @return
	 * @since 1.0
	 */
	@Transient
	public Integer getSubscriptionsCount() {
		return subscriptions.size();
	}
}
