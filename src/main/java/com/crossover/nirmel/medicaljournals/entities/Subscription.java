/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Subscription data model
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@Entity
@Table(name = "subscriptions")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Subscription extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "removed", nullable = false)
	private Boolean removed = false;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="journal_id")
	private Journal journal;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
	private User user;
	
	public Subscription() {
	}

	/**
	 * Journal id
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Journal status (active/removed)
	 * 
	 * @return
	 * @since 1.0
	 */
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	/**
	 * Subscribed journal
	 * 
	 * @return
	 * @since 1.0
	 */
	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	/**
	 * Subscribed user
	 * 
	 * @return
	 * @since 1.0
	 */
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
