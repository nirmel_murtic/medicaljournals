/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * User role data model
 * 
 * Each user may contain one or more user roles.
 * Each user role may contain zero, one or more permissions.
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@Entity
@Table(name = "roles")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "role_permissions", joinColumns = { 
			@JoinColumn(name = "role_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "permission_id", 
			nullable = false, updatable = false) })
	@OrderBy("name")
	private Set<Permission> permissions = new LinkedHashSet<Permission>();
	
	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}

	/**
	 * Permission id
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * Role name
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Permissions set
	 * Each user role may contain zero, one or more permissions.
	 * 
	 * @return
	 * @since 1.0
	 */
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}
}
