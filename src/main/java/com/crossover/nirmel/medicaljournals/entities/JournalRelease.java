/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Journal release data model
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@Entity
@Table(name = "journal_releases")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class JournalRelease extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "code", nullable = false)
	private String code;
	
	@Column(name = "location", nullable = false)
	private String location;
	
	@Column(name = "type", nullable = false)
	private String type;
	
	@Column(name = "removed", nullable = false)
	private Boolean removed = false;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="journal_id")
	private Journal journal;

	public JournalRelease() {
	}

	public JournalRelease(String code) {
		this.code = code;
	}

	/**
	 * Journal release code
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Journal release id
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Journal release status (active/removed)
	 * 
	 * @return
	 * @since 1.0
	 */
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	/**
	 * Journal source location
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Journal source type
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Referenced journal
	 * 
	 * @return
	 * @since 1.0
	 */
	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}
}
