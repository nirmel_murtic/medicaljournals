/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import java.beans.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.crossover.nirmel.medicaljournals.constants.Authority;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * User data model
 * 
 * It's used for authenticating to platform
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */

@Entity
@Table(name = "users")
@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class User extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "email", nullable = false, unique = true)
	private String email;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "password")
	@JsonProperty(access = Access.WRITE_ONLY)
	@Length(min = 6)
	private String password;
	
	@Column(name = "enabled", nullable = false)
	private Boolean enabled = true;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(name = "user_roles", joinColumns = { 
			@JoinColumn(name = "user_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "role_id", 
			nullable = false, updatable = false) })
	private Set<Role> userRoles = new HashSet<>();
	
	public User() {
	}

	/**
	 * User id
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * User email
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * User first name
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * User last name
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * User password
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Set of user roles
	 * 
	 * Each user may contain one or more user roles.
	 * 
	 * @return
	 * @since 1.0
	 */
	public Set<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<Role> userRoles) {
		this.userRoles = userRoles;
	}

	/**
	 * Information about user status (enabled/disabled)
	 * 
	 * @return
	 * @since 1.0
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * Check if user ia admin
	 * 
	 * @return
	 */
	@Transient
	public boolean isAdmin() {
		boolean isAdmin = false;
		
		for(Role role : userRoles) {
			if(role.getName().equals(Authority.ROLE_ADMIN)) {
				isAdmin = true;
			}
		}
		
		return isAdmin;
	}
}
