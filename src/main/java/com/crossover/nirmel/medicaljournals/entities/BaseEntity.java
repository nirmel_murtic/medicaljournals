/**
 * 
 */
package com.crossover.nirmel.medicaljournals.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base class for all entities
 * 
 * Auditing is implemented inside this class, 
 * each entity which extend this one will contain auditing columns.
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@CreatedDate
    @Column(name = "created_time")
    private Date createdDate = new Date();

    @LastModifiedDate
    @Column(name = "updated_time")
    private Date lastModifiedDate = new Date();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by_id", insertable=false, updatable=false)
    @JsonIgnore
    private User createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "last_modified_by_id", insertable=false, updatable=false)
    @JsonIgnore
    private User lastModifiedBy;
    
    @CreatedBy
    @Column(name = "created_by_id")
    private Long createdById;
    
    @LastModifiedBy
    @Column(name = "last_modified_by_id")
    private Long lastModifiedById;

    
    /**
     * Creation time
     * 
     * @return
     * @since 1.0
     */
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Last modification time
	 * 
	 * @return
	 * @since 1.0
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * Creation owner
	 * 
	 * @return
	 * @since 1.0
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	
	/**
	 * Last modification owner
	 * 
	 * @return
	 * @since 1.0
	 */
	public User getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(User lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Creation owner
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getCreatedById() {
		if(createdById == null && createdBy != null) {
			return createdBy.getId();
		}
		
		return createdById;
	}

	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}

	
	/**
	 * Last modification owner
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getLastModifiedById() {
		if(lastModifiedById == null && lastModifiedBy != null) {
			return lastModifiedBy.getId();
		}
		
		return lastModifiedById;
	}

	public void setLastModifiedById(Long lastModifiedById) {
		this.lastModifiedById = lastModifiedById;
	}
}
