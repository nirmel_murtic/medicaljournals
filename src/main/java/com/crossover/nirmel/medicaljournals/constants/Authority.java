/**
 * 
 */
package com.crossover.nirmel.medicaljournals.constants;

/**
 * List of available authorities
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 26, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public class Authority {
	
	// List of permissions
	public static final String PERM_MANAGE_USERS = "PERM_MANAGE_USERS";
	public static final String PERM_MANAGE_JOURNALS = "PERM_MANAGE_JOURNALS";
	public static final String PERM_UPLOAD_JOURNAL = "PERM_UPLOAD_JOURNAL";
	public static final String PERM_SUBSCRIBE_JOURNALS = "PERM_SUBSCRIBE_JOURNALS";
	
	// List of roles
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_PUBLISHER = "ROLE_PUBLISHER";
	public static final String ROLE_USER = "ROLE_USER";
	
}
