/**
 * 
 */
package com.crossover.nirmel.medicaljournals.models;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.crossover.nirmel.medicaljournals.entities.Permission;
import com.crossover.nirmel.medicaljournals.entities.Role;
import com.crossover.nirmel.medicaljournals.entities.User;

/**
 * User model used by spring security
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public class SecurityUser extends User implements UserDetails {

	private static final long serialVersionUID = 1L;
	
    public SecurityUser(User user) {
        if(user != null) {
            this.setId(user.getId());
            this.setFirstName(user.getFirstName());
            this.setLastName(user.getLastName());
            this.setEmail(user.getEmail());
            this.setPassword(user.getPassword());
            this.setUserRoles(user.getUserRoles());
        }       
    }
    
    /**
     * List of user authorities (roles + permissions)
     * 
     */
	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
		
        Collection<GrantedAuthority> authorities = new HashSet<>();
        
        Set<Role> userRoles = this.getUserRoles();
         
        for(Role userRole : userRoles) {
        	SimpleGrantedAuthority authority = new SimpleGrantedAuthority(userRole.getName());
            authorities.add(authority);
        	
	        for (Permission permission : userRole.getPermissions()) {
	            authority = new SimpleGrantedAuthority(permission.getName());
	            authorities.add(authority);
	        }
        }
        
        return authorities;
    }
 
    @Override
    public String getPassword() {
        return super.getPassword();
    }
 
    @Override
    public String getUsername() {
        return super.getEmail();
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return getEnabled();
    }  
}
