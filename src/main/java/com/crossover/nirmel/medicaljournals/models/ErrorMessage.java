/**
 * 
 */
package com.crossover.nirmel.medicaljournals.models;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerMapping;

/**
 * Class used for serializing 
 * and providing error in user friendly way 
 * 
 * <p>
 * Versions:
 * <ul>
 * <li>1.0		May 25, 2016		Nirmel						Initial Version
 * </ul>
 *
 * @author Nirmel
 *
 * @since 1.0
 * @version 1.0
 *
 */
public class ErrorMessage {
	
	private Long timestamp;
	
	private Integer status;
	
	private String error;
	
	private String exception;
	
	private String message;
	
	private String path;
	
	public ErrorMessage(Throwable e, HttpStatus status, HttpServletRequest request) {
		this.timestamp = System.currentTimeMillis();
		this.status = status.value();
		this.error = status.name();
		
		this.exception = e.getClass().getName();
		
		if(e.getMessage() != null) {
			this.message = e.getMessage();
		}
		
		if(request != null) {
			this.path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		}
	}

	/**
	 * Error timestamp
	 * 
	 * @return
	 * @since 1.0
	 */
	public Long getTimestamp() {
		return timestamp;
	}

	/**
	 * Http status code
	 * @return
	 * @since 1.0
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Http status name
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getError() {
		return error;
	}

	/**
	 * Exception class
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getException() {
		return exception;
	}

	/**
	 * Error message
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Endpoint path
	 * 
	 * @return
	 * @since 1.0
	 */
	public String getPath() {
		return path;
	}
}
