CREATE TABLE `users` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `password` VARCHAR(255) NULL,
  `enabled` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));
  
CREATE TABLE `roles` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC));
  
CREATE TABLE `permissions` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC));

CREATE TABLE `user_roles` (
  `user_id` BIGINT UNSIGNED NOT NULL,
  `role_id` BIGINT UNSIGNED NOT NULL,
  INDEX `FK_user_roles_user_id_idx` (`user_id` ASC),
  INDEX `FK_user_roles_role_id_idx` (`role_id` ASC),
  CONSTRAINT `FK_user_roles_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`),
  CONSTRAINT `FK_user_roles_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`));

CREATE TABLE `role_permissions` (
  `role_id` BIGINT UNSIGNED NOT NULL,
  `permission_id` BIGINT UNSIGNED NOT NULL,
  INDEX `FK_role_permissions_role_id_idx` (`role_id` ASC),
  INDEX `FK_role_permissions_permission_id_idx` (`permission_id` ASC),
  CONSTRAINT `FK_role_permissions_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`),
  CONSTRAINT `FK_role_permissions_permission_id`
    FOREIGN KEY (`permission_id`)
    REFERENCES `permissions` (`id`));
