ALTER TABLE `users` 
ADD COLUMN `created_time` DATETIME NULL AFTER `enabled`,
ADD COLUMN `updated_time` DATETIME NULL AFTER `created_time`,
ADD COLUMN `created_by_id` BIGINT UNSIGNED NULL AFTER `updated_time`,
ADD COLUMN `last_modified_by_id` BIGINT UNSIGNED NULL AFTER `created_by_id`,
ADD INDEX `FK_users_created_by_id_idx` (`created_by_id` ASC),
ADD INDEX `FK_users_last_modified_by_id_idx` (`last_modified_by_id` ASC);
ALTER TABLE `users` 
ADD CONSTRAINT `FK_users_created_by_id`
  FOREIGN KEY (`created_by_id`)
  REFERENCES `users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_users_last_modified_by_id`
  FOREIGN KEY (`last_modified_by_id`)
  REFERENCES `users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;