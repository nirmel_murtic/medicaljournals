INSERT INTO `users` (`email`, `first_name`, `last_name`, `password`, `enabled`) 
VALUES 	('demo@crossover.com', 'John', 'Doe', '$2a$10$iGflwpWm/Md3jdbT.5g8oOpNAyy6rB/0PlDxK3Lld02zstL4Itm7W', '1'),
		('demo_publisher@crossover.com', 'John', 'Roe', '$2a$10$iGflwpWm/Md3jdbT.5g8oOpNAyy6rB/0PlDxK3Lld02zstL4Itm7W', '1'),
		('demo_admin@crossover.com', 'Jane', 'Doe', '$2a$10$iGflwpWm/Md3jdbT.5g8oOpNAyy6rB/0PlDxK3Lld02zstL4Itm7W', '1');

INSERT INTO `roles` (`name`) VALUES ('ROLE_USER');
INSERT INTO `roles` (`name`) VALUES ('ROLE_PUBLISHER');
INSERT INTO `roles` (`name`) VALUES ('ROLE_ADMIN');

INSERT INTO `permissions` (`name`) VALUES ('PERM_MANAGE_JOURNALS');
INSERT INTO `permissions` (`name`) VALUES ('PERM_MANAGE_USERS');
INSERT INTO `permissions` (`name`) VALUES ('PERM_UPLOAD_JOURNAL');
INSERT INTO `permissions` (`name`) VALUES ('PERM_SUBSCRIBE_JOURNALS');

/* Handle ROLE_USER permissions */
INSERT INTO `role_permissions` (`role_id`, `permission_id`) 
VALUES ((SELECT id FROM roles where name = 'ROLE_USER'), 
		(SELECT id FROM permissions where name = 'PERM_SUBSCRIBE_JOURNALS'));
		
/* Handle ROLE_MANAGER permissions */
INSERT INTO `role_permissions` (`role_id`, `permission_id`) 
VALUES ((SELECT id FROM roles where name = 'ROLE_PUBLISHER'), 
		(SELECT id FROM permissions where name = 'PERM_MANAGE_JOURNALS')),
		((SELECT id FROM roles where name = 'ROLE_PUBLISHER'),
		(SELECT id FROM permissions where name = 'PERM_UPLOAD_JOURNAL'));
		
/* Handle ROLE_ADMIN permissions */
INSERT INTO `role_permissions` (`role_id`, `permission_id`) 
VALUES ((SELECT id FROM roles where name = 'ROLE_ADMIN'), 
		(SELECT id FROM permissions where name = 'PERM_MANAGE_USERS'));
		
/* Assing user roles */
INSERT INTO `user_roles` (`user_id`, `role_id`) 
VALUES ((SELECT id FROM users where email = 'demo@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_USER')),
		((SELECT id FROM users where email = 'demo_publisher@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_USER')),
		((SELECT id FROM users where email = 'demo_publisher@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_PUBLISHER')),
		((SELECT id FROM users where email = 'demo_admin@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_USER')),
		((SELECT id FROM users where email = 'demo_admin@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_PUBLISHER')),
		((SELECT id FROM users where email = 'demo_admin@crossover.com'), 
		(SELECT id FROM roles where name = 'ROLE_ADMIN'));