CREATE TABLE `journal_releases` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `location` VARCHAR(255) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `created_time` DATETIME NULL,
  `updated_time` DATETIME NULL,
  `created_by_id` BIGINT(20) UNSIGNED NULL,
  `last_modified_by_id` BIGINT(20) UNSIGNED NULL,
  `removed` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  INDEX `FK_journal_releases_created_by_id_idx_idx` (`created_by_id` ASC),
  INDEX `FK_journal_releases_last_modified_by_id_idx_idx` (`last_modified_by_id` ASC),
  CONSTRAINT `FK_journal_releases_created_by_id_idx`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_journal_releases_last_modified_by_id_idx`
    FOREIGN KEY (`last_modified_by_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `journal_releases` 
ADD COLUMN `journal_id` BIGINT(20) UNSIGNED NOT NULL AFTER `removed`,
ADD INDEX `FK_journal_releases_journal_id_idx` (`journal_id` ASC);
ALTER TABLE `journal_releases` 
ADD CONSTRAINT `FK_journal_releases_journal_id`
  FOREIGN KEY (`journal_id`)
  REFERENCES `journals` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
