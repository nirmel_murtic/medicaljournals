CREATE TABLE `subscriptions` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `journal_id` BIGINT(20) UNSIGNED NOT NULL,
  `created_time` DATETIME NULL,
  `updated_time` DATETIME NULL,
  `created_by_id` BIGINT(20) UNSIGNED NULL,
  `last_modified_by_id` BIGINT(20) UNSIGNED NULL,
  `removed` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  INDEX `FK_subscriptions_created_by_id_idx_idx_idx` (`created_by_id` ASC),
  INDEX `FK_subscriptions_last_modified_by_id_idx_idx_idx` (`last_modified_by_id` ASC),
  INDEX `FK_subscriptions_user_id_idx` (`user_id` ASC),
  INDEX `FK_subscriptions_journal_id_idx` (`journal_id` ASC),
  CONSTRAINT `FK_subscriptions_created_by_id_idx_idx`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_subscriptions_last_modified_by_id_idx_idx`
    FOREIGN KEY (`last_modified_by_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_subscriptions_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_subscriptions_journal_id`
    FOREIGN KEY (`journal_id`)
    REFERENCES `journals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
