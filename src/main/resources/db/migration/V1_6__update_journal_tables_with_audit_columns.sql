ALTER TABLE `journals` 
ADD COLUMN `created_time` DATETIME NULL DEFAULT NULL AFTER `description`,
ADD COLUMN `updated_time` DATETIME NULL DEFAULT NULL AFTER `created_time`,
ADD COLUMN `created_by_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `updated_time`,
ADD COLUMN `last_modified_by_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `created_by_id`,
ADD COLUMN `removed` BIT(1) NOT NULL DEFAULT b'0' AFTER `last_modified_by_id`,
ADD INDEX `FK_journals_created_by_id_idx_idx` (`created_by_id` ASC),
ADD INDEX `FK_journals_last_modified_by_id_idx_idx` (`last_modified_by_id` ASC);
ALTER TABLE `journals` 
ADD CONSTRAINT `FK_journals_created_by_id_idx`
  FOREIGN KEY (`created_by_id`)
  REFERENCES `users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_journals_last_modified_by_id_idx`
  FOREIGN KEY (`last_modified_by_id`)
  REFERENCES `users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;