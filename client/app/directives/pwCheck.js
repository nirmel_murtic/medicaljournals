'use strict';

journalsApp.directive('pwCheck', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
          var firstPassword = '#' + attrs.pwCheck;

          elem.on('keyup', function() {
            var passwordOk = elem.val() === $(firstPassword).val() || !$(firstPassword).val().length;

            if(passwordOk) {
              scope.$apply(function () {
                ctrl.$setValidity('pwmatch', passwordOk);
              });
            }
          });

          $(document).on('keyup', firstPassword, function() {
            var passwordOk = elem.val() === $(firstPassword).val() || !elem.val().length;

            if(passwordOk) {
              scope.$apply(function () {
                ctrl.$setValidity('pwmatch', passwordOk);
              });
            }
          });

          elem.on('change', function() {
            var passwordOk = elem.val() === $(firstPassword).val() || !$(firstPassword).val().length;

            scope.$apply(function () {
              ctrl.$setValidity('pwmatch', passwordOk);
            });
          });

          $(document).on('change', firstPassword, function() {
            var passwordOk = elem.val() === $(firstPassword).val() || !elem.val().length;

            scope.$apply(function () {
              ctrl.$setValidity('pwmatch', passwordOk);
            });
          });
        }
    };
  });
