 'use strict';

var journalsApp = angular.module('journalsApp', ['templates', 'ngRoute', 'base64', 'angularUtils.directives.dirPagination', 'ui.bootstrap.showErrors', 'ngFileUpload'])
	.run(function(configuration, $rootScope, loginService, userService, $route, $location, $http, sideBarMenu) {
			$rootScope.selectMenu = function(location) {
				if(!$rootScope.sideBarMenu) {
					$rootScope.sideBarMenu = sideBarMenu;
				}
				
				var result = null;
				
				angular.forEach($rootScope.sideBarMenu, function(value, key) {
					value.active = location === key;
					
					if(value.active) {
						result = value;
					}
				});
				
				return result;
			};
		
			$rootScope.onLogin = function(token) {
				$rootScope.authenticated = true;
				
				$http.defaults.headers.common.authorization = 'Bearer ' + token;

				if($route.current.$$route.originalPath == "/login") {
                    $location.path("/journalSubscriptions");
                } else if($rootScope.currentMenu && !$rootScope.hasAnyPermission($rootScope.currentMenu.permissions)) {
	        		$location.path( "/journalSubscriptions" );
	        	}
				
				$rootScope.updateSearchVisibility();
			};
			
			$rootScope.hasAnyPermission = function(permissions) {
				if(!permissions) return true;
				
				var result = false;
				
				angular.forEach($rootScope.userInfo.authorities, function(value, key) {
					if(permissions.indexOf(value) !== -1) {
						result = true;
						
						return false;
					}
				});
				
				return result;
			};
			
			$rootScope.logIn = function() {
				$location.path("/login");
			};
			
			$rootScope.logOut = function() {
				$rootScope.authenticated = false;
				
				delete localStorage['access_token'];
				delete localStorage['refresh_token'];
				
				delete $http.defaults.headers.common.Authorization;
				
				if($location.path() !== '/register') {
					$location.path("/login");
				}
			};
			
			$rootScope.$on("$routeChangeStart", function (event, next, current) {
		          if(current && !$rootScope.authenticated) {
		              if(next.$$route.originalPath != '/login' && next.$$route.originalPath != '/register') {
		                  $location.path( "/login" );
		              }
		          } else if($rootScope.authenticated && next.$$route && next.$$route.originalPath == '/login') {
		              event.preventDefault();
		          }
		          
		          if(next && next.$$route) {
		        	  $rootScope.currentMenu = $rootScope.selectMenu(next.$$route.originalPath);
		          }
		          
		          if($rootScope.userInfo && $rootScope.currentMenu && !$rootScope.hasAnyPermission($rootScope.currentMenu.permissions)) {
		        		$location.path( "/journalSubscriptions" );
		          }
		          
		          $rootScope.updateSearchVisibility();
	        });
			
			if(localStorage['access_token'] || localStorage['refresh_token']) {
				var refreshToken = function() {
					if(localStorage['refresh_token']) {
		    			 // If stored access token is not valid but refresh token exist, try to refresh token
						loginService.refreshToken(localStorage['refresh_token']).then(function(result) {
								var data = result.data;
								
		    		    	 	console.log('Access token is refreshed successfully, user is logged in.');
		    			        
		    		    	 	$rootScope.userInfo = data;
		    		    	 	
		    					localStorage['access_token'] = data['access_token'];
		    					localStorage['refresh_token'] = data['refresh_token'];
		    					
		    					$rootScope.onLogin(data['access_token']);
		    		     }, function(result) {
		    		    	 	var error = result.data;
		    		    	 
		    		    	 	console.log('Error while refreshing token: ' + error);
		    		    	 	
		    		    	 	$rootScope.logOut();
		    		     });
		    		 } else {
		    			 $rootScope.logOut();
		    		 }
				};
				
				if(localStorage['access_token']) {
					// Check if stored access token is valid
					loginService.checkToken(localStorage['access_token']).then(function(result) {
						 var data = result.data; 
						
				    	 console.log('Stored access token is valid, user is logged in.');
				    	 
				    	 // If stored access token is valid, authenticate user
				    	 $rootScope.userInfo = data;
				    	 
				    	 $rootScope.onLogin(localStorage['access_token']);
				     }, function(error) {
				    	 console.log('Invalid access token, try to refresh it.');
				    	 
				    	 refreshToken();
				     });
				} else {
					refreshToken();
				}
			} else {
				$rootScope.logOut();
			}
			
			$rootScope.updateSearchVisibility = function() {
				$rootScope.searchVisible = $rootScope.authenticated && 
					($location.path().indexOf('/journals', $location.path().length - 9) !== -1 ||
							$location.path().indexOf('/users') !== -1 ||
							$location.path().indexOf('/journalSubscriptions') !== -1);
			}
		}
	)
	.config(function(showErrorsConfigProvider) {
	  showErrorsConfigProvider.showSuccess(true);
	});