 'use strict';
 
journalsApp.constant('sideBarMenu', {
	'separator1': {
		separator: true,
		name: 'Journals',
		permissions: [ 'PERM_SUBSCRIBE_JOURNALS' ]
	},
	'/journals': {
		name: 'My Journals',
    	active: true,
    	permissions: [ 'PERM_MANAGE_JOURNALS' ]
	},
	'/journalSubscriptions': {
		name: 'Journal Subscriptions',
    	permissions: [ 'PERM_SUBSCRIBE_JOURNALS' ]
	},
	'/addJournal': {
		name: 'Add Journal',
		permissions: [ 'PERM_MANAGE_JOURNALS' ]
	},
	'separator2': {
		separator: true,
		name: 'User management',
		permissions: [ 'PERM_MANAGE_USERS' ]
	},
	'/users': {
		name: 'Users',
		permissions: [ 'PERM_MANAGE_USERS' ]
	}
});