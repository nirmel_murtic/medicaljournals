'use strict';

journalsApp.controller('JournalsController', function($rootScope, $scope, journalService, $interval, $location, $routeParams, user) {	
	$scope.journalsPage = {
			number: 1
	};
	$scope.pageSize = 3;
	$scope.sortColumn = null;

	if($routeParams.userId) {
		$scope.userId = $routeParams.userId;
	} else {
		$scope.userId = user.id;
	}
	
	$scope.onSort = function(sortColumn) {
		if($scope.sortColumn != sortColumn) {
			$scope.sortType = 'asc';
		} else {
			$scope.sortType = $scope.sortType === 'asc' ? 'desc' : 'asc';
		}
		
		$scope.sortColumn = sortColumn;
		
		$scope.loadPage($scope.journalsPage.number - 1);
	};
	
	$rootScope.onSearch = function() {
		$scope.loadPage($scope.journalsPage.number - 1);
	};
	
	$scope.loadPage = function(num) {
		journalService.getJournals(num, $scope.pageSize, $scope.sortColumn, $rootScope.searchValue, $scope.sortType, $scope.userId).then(function(result) {			
			$scope.journalsPage.content = result.data.content;
	    	$scope.journalsPage.totalElements = result.data.totalElements;
	    	$scope.journalsPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.journalsPage.size;
	    }, function(result) {
	    	console.log('Error while loading journals: ' + result.data.error.message);
	    });
	};
	
	$scope.loadPage(0);
    
    $scope.editJournal = function(journal) {
    	$location.path("/journals/update/" + journal.id);
    };
    
	$scope.openJournal = function(journal) {
		$location.path("/journals/view/" + journal.id);
	};
    
    $scope.removeJournal = function(journal) {
    	swal({
    		  title: "Are you sure?",
    		  text: "Your will not be able to recover this process!",
    		  type: "warning",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-danger",
    		  confirmButtonText: "Yes, delete it!",
    		  closeOnConfirm: false
    		},
    		function(){
    			journalService.removeJournal(journal.id).then(function(result) {
    	        	var index = $scope.journalsPage.content.indexOf(journal);
    	        	
    	        	if(index !== -1) {
    	        		$scope.journalsPage.content.splice(index, 1);
    	        		
    	        		swal("Deleted!", "Journal is deleted succesfully.", "success");
    	        	}
    	    	}, function(result) {
    	    		var error = result.data;
    	    		
    	    		swal("Error", error && error['error_description'] ? error['error_description'] : 'Error while deleting journal', "error");
    	    	});
    		});
    };
  });