'use strict';

journalsApp.controller('LoginController', function($rootScope, $scope, loginService) {
    var self = this;
    
    $scope.login = function() {
    	loginService.logIn($scope.email, $scope.password).then(function(result) {
    		  var data = result.data
    		
			  if (data && data['access_token']) {
				console.log('User is logged in successfully');
				
				localStorage['access_token'] = data['access_token'];
				localStorage['refresh_token'] = data['refresh_token'];
				
				loginService.checkToken(localStorage['access_token']).then(function(result) {
					 var data = result.data; 
					 
					 $rootScope.userInfo = data;
			    	 
			    	 $rootScope.onLogin(localStorage['access_token']);
			     }, function(error) {
			    	 console.log('Invalid access token.');
			    	 
			    	 $rootScope.logOut();
			     });
		      } else {
		    	$rootScope.logOut();
		      }
		 }, function(result) {
			  var error = result.data;
			  
			  $rootScope.logOut();
		      
		      swal("Error", error && error['error_description'] ? error['error_description'] : 'Error while logging in', "error");
		 });
    };
  });