'use strict';

journalsApp.controller('UpdateProfileController', function($rootScope, $scope, userService, $window, user) {
    $scope.profile = {
    	firstName: user.firstName,
    	lastName: user.lastName,
    	email: user.email,
    	id: user.id
    };
    
	$scope.updateProfile = function() {
		delete $scope.formUpdateProfile.email.$error.alreadyExist;
		
		userService.updateProfile($scope.profile, $scope.currentPassword).then(function(result) {
			$window.history.back();
    	}, function(result) {
    		var error = result.data;
    		
    		var message = 'Error while updating profile';
    		
    		if(error.exception == 'org.hibernate.metamodel.ValidationException') {
    			$scope.formUpdateProfile.currentPassword.$error.wrongPassword = true;
				$scope.formUpdateProfile.currentPassword.$invalid = true;
				
				$scope.$broadcast('show-errors-check-validity');
    		} else if(error.status == 409) {
    			$scope.formUpdateProfile.email.$error.alreadyExist = true;
				$scope.formUpdateProfile.email.$invalid = true;
				
				$scope.$broadcast('show-errors-check-validity');
    		} else {
    			swal("Error", message, "error");
    		}
    	});
    };
  });