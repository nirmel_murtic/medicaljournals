'use strict';

journalsApp.controller('AddOrUpdateJournalController', function($scope, journalService, $routeParams, $window, $location) {
    $scope.journal = {};
    $scope.release = {};
    $scope.pageSize = 5;
    
    $scope.releasesPage = {
		number: 1
	};
    
    $scope.loadReleases = function(num) {
    	journalService.getJournalReleases($routeParams.id, num, $scope.pageSize).then(function(result) {
    		$scope.releasesPage.content = result.data.content;
	    	$scope.releasesPage.totalElements = result.data.totalElements;
	    	$scope.releasesPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.releasesPage.size;
    	}, function(result) {
    		swal("Error", 'Error while loading journal releases #' + $routeParams.id, "error");
    	});
	};
    
    if($routeParams.id) {
    	journalService.getJournal($routeParams.id).then(function(result) {
    		$scope.journal = result.data;
    	}, function(result) {
    		swal("Error", 'Error while loading journal #' + $routeParams.id, "error");
    	});
    	
    	$scope.loadReleases(0);
    }
	
	$scope.addOrUpdateJournal = function() {
		journalService.addOrUpdateJournal($scope.journal).then(function(result) {
    		$location.path("/journals/update/" + result.data.id).replace();
    	}, function(result) {
    		var error = result.data;
    		
    		swal("Error", 'Error while saving journal', "error");
    	});
    };
    
    $scope.addRelease = function() {
    	$scope.release.journalId = $scope.journal.id;
    	
    	if(!$scope.release.releaseFile) {
    		return;
    	}
    	
    	journalService.addRelease($scope.release.releaseFile, $scope.release).then(function(result) {
    		swal("Added!", "Journal release added successfully.", "success");
    		
    		$scope.loadReleases(0);
    		
    		$scope.release = {};
    	}, function(result) {
    		var error = result.data;
    		
    		swal("Error", 'Error while saving release', "error");
    	});
    };
    
    $scope.removeRelease = function(event, release) {
    	event.preventDefault();
     		
		swal({
      		  title: "Are you sure?",
      		  text: "Your will not be able to recover this process!",
      		  type: "warning",
      		  showCancelButton: true,
      		  confirmButtonClass: "btn-danger",
      		  confirmButtonText: "Yes, delete it!",
      		  closeOnConfirm: false
      		},
      		function(){
      	    	journalService.removeRelease(release.id).then(function(result) {   
      	    		$scope.loadReleases($scope.releasesPage.number - 1);
      	    		
      				swal("Deleted!", "Journal release is deleted succesfully.", "success");
      	    	}, function(result) {
      	    		var error = result.data;
      	    		
      	    		swal("Error", 'Error while removing journal release', "error");
      	    	});
      		});
    };
  });