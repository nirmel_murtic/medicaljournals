'use strict';

journalsApp.controller('ViewJournalSourceController', function($scope, journalService, $routeParams, configuration) {
    $scope.release = {};
    
    if($routeParams.id) {
        $scope.sourceUrl = configuration.apiUrl + 'api/releases/' + $routeParams.id + '/source?access_token=' + localStorage['access_token'];
    	
    	journalService.getJournalRelease($routeParams.id).then(function(result) {
    		$scope.release = result.data;
    	}, function(result) {
    		swal("Error", 'Error while loading journal release #' + $routeParams.id, "error");
    	});
    }
  });