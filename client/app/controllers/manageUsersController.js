'use strict';

journalsApp.controller('ManageUsersController', function($rootScope, $scope, userService, $location) {
	$scope.usersPage = {
			number: 1
	};
	$scope.pageSize = 3;
	$scope.sortColumn = null;
	
	$scope.onSort = function(sortColumn) {
		if($scope.sortColumn != sortColumn) {
			$scope.sortType = 'asc';
		} else {
			$scope.sortType = $scope.sortType === 'asc' ? 'desc' : 'asc';
		}
		
		$scope.sortColumn = sortColumn;
		
		$scope.loadPage($scope.usersPage.number - 1);
	};
	
	$rootScope.onSearch = function() {
		$scope.loadPage($scope.usersPage.number - 1);
	};
	
	$scope.loadPage = function(num) {
		userService.getAllUsers(num, $scope.pageSize, $scope.sortColumn, $rootScope.searchValue, $scope.sortType).then(function(result) {			
			$scope.usersPage.content = result.data.content;
	    	$scope.usersPage.totalElements = result.data.totalElements;
	    	$scope.usersPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.usersPage.size;
	    }, function(result) {
	    	console.log('Error while loading users: ' + result.data.error.message);
	    });
	};
	
	$scope.loadPage(0);
	
	$scope.editUser = function(user) {
    	$location.path("/users/" + user.id);
    };
    
    $scope.viewJournals = function(user) {
    	$location.path("/users/" + user.id + '/journals');
    }
  });