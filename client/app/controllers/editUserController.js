'use strict';

journalsApp.controller('EditUserController', function($rootScope, $scope, userService, $window, $routeParams, roleService) {
    $scope.user = {};
    
    if($routeParams.id) {
    	userService.getUser($routeParams.id).then(function(result) {
    		$scope.user = result.data;
    		
    		$scope.getRoles();
    	}, function(result) {
    		swal("Error", 'Error while loading user #' + $routeParams.id, "error");
    	})
    }
	
	$scope.updateUser = function() {
		delete $scope.formUpdateUser.email.$error.alreadyExist;
		
		userService.updateUser($scope.user).then(function(result) {
			$window.history.back();
    	}, function(result) {
    		var error = result.data;
    		
    		var message = 'Error while saving user';
    		
    		if(error.exception == 'org.hibernate.metamodel.ValidationException') {
    			message = error.message;
    		}
    		
    		if(error.status == 409) {
    			$scope.formUpdateUser.email.$error.alreadyExist = true;
				$scope.formUpdateUser.email.$invalid = true;
				
				$scope.$broadcast('show-errors-check-validity');
    		} else {
    			swal("Error", message, "error");
    		}
    	});
    };
    
    $scope.getRoles = function() {
	    if($rootScope.hasAnyPermission('ROLE_ADMIN')) {
		    roleService.getRoles().then(function(result) {
		    	$scope.availableRoles = result.data;
		    	
		    	angular.forEach($scope.availableRoles, function(role, key) {
		    		delete role.permissions;
		    	});
		    	
		    	angular.forEach($scope.user.userRoles, function(role, key) {
		    		delete role.permissions;
		    		
		    		var filteredRole = $scope.availableRoles.filter(function(value) {
		    			return value.name === role.name;
		    		})[0];
		    		
		    		filteredRole.checked = true;
		    	});
		    }, function(result) {
		    	console.log('Error while loading roles');
		    });
	    } else {
	    	$scope.availableRoles = $scope.user.userRoles;
	    	
	    	angular.forEach($scope.availableRoles, function(role, key) {
	    		delete role.permissions;
	    		role.checked = true;
	    	});
	    }
    };
    
    $scope.onRoleClicked = function(role) {
    	if(role.checked) {
    		$scope.user.userRoles.push(role);
    	} else {
    		var filteredRole = $scope.user.userRoles.filter(function(value) {
    			return value.name === role.name;
    		})[0];
    		
    		if(filteredRole) {
    			var index = $scope.user.userRoles.indexOf(filteredRole);
    			
    			if(index !== -1) {
    				$scope.user.userRoles.splice(index, 1);
    			}
    		}
    	}
    };
  });