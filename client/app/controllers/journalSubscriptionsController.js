'use strict';

journalsApp.controller('JournalSubscriptionsController', function($rootScope, $scope, journalService, $interval, $location, $routeParams) {	
	$scope.subscribedJournalsPage = {
			number: 1
	};
	
	$scope.unsubscribedJuornalsPage = {
			number: 1
	};
	$scope.pageSize = 5;
	$scope.sortColumn = null;
	$scope.userId = $routeParams.userId;
	
	$scope.onSort = function(sortColumn) {
		if($scope.sortColumn != sortColumn) {
			$scope.sortType = 'asc';
		} else {
			$scope.sortType = $scope.sortType === 'asc' ? 'desc' : 'asc';
		}
		
		$scope.sortColumn = sortColumn;
	};
	
	$rootScope.onSearch = function() {
		$scope.loadSubscribedJournals($scope.subscribedJournalsPage.number - 1);
		$scope.loadUnsubscribedJournals($scope.unsubscribedJuornalsPage.number - 1);
	};
	
	$scope.loadSubscribedJournals = function(num) {
		journalService.getSubscribedJournals(num, $scope.pageSize, $scope.sortColumn, $rootScope.searchValue, $scope.sortType, $scope.userId).then(function(result) {			
			$scope.subscribedJournalsPage.content = result.data.content;
	    	$scope.subscribedJournalsPage.totalElements = result.data.totalElements;
	    	$scope.subscribedJournalsPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.subscribedJournalsPage.size;
	    }, function(result) {
	    	console.log('Error while loading subscribed journals: ' + result.data.error.message);
	    });
	};
	
	$scope.loadUnsubscribedJournals = function(num) {
		journalService.getUnsubscribedJournals(num, $scope.pageSize, $scope.sortColumn, $rootScope.searchValue, $scope.sortType, $scope.userId).then(function(result) {			
			$scope.unsubscribedJuornalsPage.content = result.data.content;
	    	$scope.unsubscribedJuornalsPage.totalElements = result.data.totalElements;
	    	$scope.unsubscribedJuornalsPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.unsubscribedJuornalsPage.size;
	    }, function(result) {
	    	console.log('Error while loading unsubscribed journals: ' + result.data.error.message);
	    });
	};
	
	$scope.subscribeJournal = function(journal) {
		journalService.subscribeToJournal(journal.id).then(function(result) {			
			$scope.loadSubscribedJournals($scope.subscribedJournalsPage.number - 1);
			$scope.loadUnsubscribedJournals($scope.unsubscribedJuornalsPage.number - 1);
			
			swal("Subscribed!", "You are subscribed to journal.", "success");
	    }, function(result) {
	    	console.log('Error while loading unsubscribed journals: ' + result.data.error.message);
	    });
	};
	
	$scope.unsubscribeJournal = function(journal) {
		journalService.unsubscribeFromJournal(journal.id).then(function(result) {			
			$scope.loadSubscribedJournals($scope.subscribedJournalsPage.number - 1);
			$scope.loadUnsubscribedJournals($scope.unsubscribedJuornalsPage.number - 1);
			
			swal("Unsubscribed!", "You are unsubscribed from journal.", "success");
	    }, function(result) {
	    	console.log('Error while loading unsubscribed journals: ' + result.data.error.message);
	    });
	};
	
	$scope.openJournal = function(journal) {
		$location.path("/journals/view/" + journal.id);
	};
	
	$scope.loadSubscribedJournals(0);
	$scope.loadUnsubscribedJournals(0);
  });