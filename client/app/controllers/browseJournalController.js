'use strict';

journalsApp.controller('BrowseJournalController', function($scope, journalService, $routeParams) {
    $scope.journal = {};
    $scope.pageSize = 5;
    
    $scope.releasesPage = {
		number: 1
	};
    
    $scope.loadReleases = function(num) {
    	journalService.getJournalReleases($routeParams.id, num, $scope.pageSize).then(function(result) {
    		$scope.releasesPage.content = result.data.content;
	    	$scope.releasesPage.totalElements = result.data.totalElements;
	    	$scope.releasesPage.size = result.data.size;
	    	
	    	$scope.pageSize = $scope.releasesPage.size;
    	}, function(result) {
    		swal("Error", 'Error while loading journal releases #' + $routeParams.id, "error");
    	});
	};
    if($routeParams.id) {
    	journalService.getJournal($routeParams.id).then(function(result) {
    		$scope.journal = result.data;
    	}, function(result) {
    		swal("Error", 'Error while loading journal #' + $routeParams.id, "error");
    	});
    	
    	$scope.loadReleases(0);
    }
  });