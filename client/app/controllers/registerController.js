'use strict';

journalsApp.controller('RegisterController', function($rootScope, $scope, userService, $location) {
	$scope.user = {};
	
	$scope.register = function() {
		delete $scope.formRegister.email.$error.alreadyExist;
		
		userService.register($scope.user).then(function(result) {
			$location.path("/login");
			
			swal("Done", 'User is registered successfully', "success");
		}, function(result) {
			console.log('Error while registering user: ' + result.data.error.message);
			
			if(result.data.status === 409) {
				$scope.formRegister.email.$error.alreadyExist = true;
				$scope.formRegister.email.$invalid = true;
				
				$scope.$broadcast('show-errors-check-validity');
			} else {
				swal("Error", 'Error while registering user', "error");
			}
		});
	};
  });