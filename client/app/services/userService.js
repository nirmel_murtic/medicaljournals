'use strict';

journalsApp.factory('userService', function ($http, configuration) {
    return {
    	getCurrentUser: function() {
    		return $http({
    	        url: 'api/users/currentUser', 
    	        method: "GET"
    	     });
    	},
    	register: function(user) {
    		return $http({
    	        url: 'api/users/register', 
    	        data: user,
    	        method: "POST"
    	     });
    	},
    	getAllUsers: function(page, size, sortColumn, searchValue, sortType) {
    		return $http({
    	        url: 'api/users',
    	        params: {
    	        	page: page,
    	        	size: size,
    	        	sort: sortColumn ? sortColumn + ',' + sortType : null,
    	        	search: searchValue
    	        },
    	        method: "GET"
    	     });
    	},
    	getUser: function(id) {
    		return $http({
    	        url: 'api/users/' + id, 
    	        method: "GET"
    	     });
    	},
    	updateUser: function(user) {
    		return $http({
    	        url: 'api/users', 
    	        data: user,
    	        method: "POST"
    	     });
    	},
    	updateProfile: function(user, currentPassword) {
    		return $http({
    	        url: 'api/users/updateProfile', 
    	        data: user,
    	        params: {
    	        	currentPassword: currentPassword
    	        },
    	        method: "POST"
    	     });
    	}
    }
});