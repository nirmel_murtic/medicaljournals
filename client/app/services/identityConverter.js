'use strict';

/**
 * This factory is responsible for conversion of json structure generated using @JsonIdentityInfo on backend side
 * to javascript object structure. It will change all key references with real javascript objects.
 * This is used to prevent recursion issues while generating json files on backend side.
 * 
 */

journalsApp.factory('identityConverter', function () {
    var getKeys = function(obj, key) {
        var keys = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) {
              continue;
            }
            if (typeof obj[i] === 'object') {
                keys = keys.concat(getKeys(obj[i], key));
            } else if (i === key) {
                keys.push( { key: obj[key], obj: obj });
            }
        }

        return keys;
    };

    var convertToObject = function(obj, key, keys) {
        if(!keys) {
            keys = getKeys(obj, key);

            var convertedKeys = {};

            for(var i = 0; i<keys.length; i++) {
                convertedKeys[keys[i].key] = keys[i].obj;
            }

            keys = convertedKeys;
        }

        for (var j in obj) {
            if (!obj.hasOwnProperty(j)) {
              continue;
            }
            if (typeof obj[j] === 'object') {
            	convertToObject(obj[j], key, keys);
            } else if( j === key) {
                delete obj[j];
            } else if (keys[obj[j]]) {
                obj[j] = keys[obj[j]];
            }
        }

        return obj;
    };
    
    var convertToJson = function(obj, key, objects) {
    	var guid = function() {
    		function s4() {
    			return Math.floor((1 + Math.random()) 
					    		* 0x10000).toString(16).substring(1);
    		}
    
    		return s4() + s4() + '-' + s4() + '-' + s4() 
	    		+ '-' + s4() + '-' + s4() + s4() + s4();
    	}
    
    	if(!objects) {
    		objects = [];
    
    		if (typeof obj === 'object' && 
	    		Object.prototype.toString.call( obj ) !== '[object Array]') {
    			obj[key] = guid();
    
    			objects.push(obj);
    		}
    	}
    
    	for (var i in obj) {
    		if (!obj.hasOwnProperty(i)) {
    		  	continue;
    		}
    
    		if (obj[i] && typeof obj[i] === 'object') {
    		    var objIndex = objects.indexOf(obj[i]);
    		    if(objIndex === -1) {
    				if(Object.prototype.toString.call( obj[i] ) !== '[object Array]') {
    					obj[i][key] = guid();
    
    					objects.push(obj[i]);
    				}
    
    				convertToJson(obj[i], key, objects);
    			} else {
    				obj[i] = objects[objIndex][key];
    			}
    		}
    	}
    
    	return obj;
    }

    return {
        toObject: function (object, key) {
            return convertToObject(angular.copy(object), key);
        },
        toJson: function (object, key) {
            return convertToJson(angular.copy(object), key);
        }
    };
});