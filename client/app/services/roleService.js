'use strict';

journalsApp.factory('roleService', function ($http, configuration) {
    return {
    	getRoles: function() {
    		return $http({
    	        url: 'api/roles',
    	        method: "GET"
    	     });
    	}
    }
});