'use strict';

journalsApp.factory('journalService', function ($http, configuration, Upload) {
    return {
    	getJournals: function(page, size, sortColumn, searchValue, sortType, userId) {
    		return $http({
    	        url: 'api/journals',
    	        params: {
    	        	page: page,
    	        	size: size,
    	        	sort: sortColumn ? sortColumn + ',' + sortType : null,
    	        	name: searchValue,
    	        	userId: userId
    	        },
    	        method: "GET"
    	     });
    	},
    	getSubscribedJournals: function(page, size, sortColumn, searchValue, sortType, userId) {
    		return $http({
    	        url: 'api/journals/subscribedJournals',
    	        params: {
    	        	page: page,
    	        	size: size,
    	        	sort: sortColumn ? sortColumn + ',' + sortType : null,
    	        	name: searchValue,
    	        	userId: userId
    	        },
    	        method: "GET"
    	     });
    	},
    	getUnsubscribedJournals: function(page, size, sortColumn, searchValue, sortType, userId) {
    		return $http({
    	        url: 'api/journals/unsubscribedJournals',
    	        params: {
    	        	page: page,
    	        	size: size,
    	        	sort: sortColumn ? sortColumn + ',' + sortType : null,
    	        	name: searchValue,
    	        	userId: userId
    	        },
    	        method: "GET"
    	     });
    	},
    	getJournal: function(id) {
    		return $http({
    	        url: 'api/journals/' + id, 
    	        method: "GET"
    	     });
    	},
    	addOrUpdateJournal: function(journal) {
    		return $http({
    	        url: 'api/journals', 
    	        data: {
    	        	id: journal.id,
    	        	name: journal.name,
    	        	description: journal.description
    	        },
    	        method: "POST"
    	     });
    	},
    	removeJournal: function(id) {
    		return $http({
    	        url: 'api/journals/' + id,
    	        method: "DELETE"
    	     });
    	},
    	removeRelease: function(id) {
    		return $http({
    	        url: 'api/releases/' + id,
    	        method: "DELETE"
    	     });
    	},
    	subscribeToJournal: function(journalId) {
    		return $http({
    	        url: 'api/journals/' + journalId + '/subscribe',
    	        method: "POST"
    	     });
    	},
    	unsubscribeFromJournal: function(journalId) {
    		return $http({
    	        url: 'api/journals/' + journalId + '/unsubscribe',
    	        method: "POST"
    	     });
    	},
    	addRelease: function(file, release) {
    		return Upload.upload({
                url: 'api/releases',
                data: {
                	file: file,
                	releaseCode: release.code,
                	journalId: release.journalId
                }
            });
    	},
    	getJournalRelease: function(id) {
    		return $http({
    	        url: 'api/releases/' + id, 
    	        method: "GET"
    	     });
    	},
    	getJournalReleases: function(id, page, size) {
    		return $http({
    	        url: 'api/journals/' + id + '/releases', 
    	        method: "GET",
    	        params: {
    	        	page: page,
    	        	size: size,
    	        	sort: 'id,desc',
    	        }
    	     });
    	}
    }
});
