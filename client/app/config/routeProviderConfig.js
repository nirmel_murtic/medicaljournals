 'use strict';
 
journalsApp.config(function($routeProvider) {
    var user = function(userService, $rootScope, $interval, $q) {
    	var deferred = $q.defer();
    	
    	var promise = $interval(function() {
    		if(!$rootScope.authenticated) {
    			return false;
    		}
    		
    		$interval.cancel(promise);
    		
    		return userService.getCurrentUser().then(function(result){
				$rootScope.user = result.data;
				
				deferred.resolve($rootScope.user);
			}, function(result) {
				var error = result.data;
				
				deferred.reject(error && error['message'] ? error['message'] : 'Error while loading user');
				
				swal("Error while loading user", error && error['message'] ? error['message'] : '', "error")
				$rootScope.logOut();
			});
    	}, 100);
    	
    	return deferred.promise;
    }

    $routeProvider.
      when('/journals', {
        templateUrl: 'app/views/journalsView.tpl.html',
        controller: 'JournalsController',
        resolve: {
        	user: user
        }
      }).
      when('/journalSubscriptions', {
            templateUrl: 'app/views/journalSubscriptionsView.tpl.html',
            controller: 'JournalSubscriptionsController',
            resolve: {
            	user: user
            }
          }).
      when('/users', {
        templateUrl: 'app/views/manageUsersView.tpl.html',
        controller: 'ManageUsersController',
        resolve: {
        	user: user
        }
      }).
      when('/users/:id', {
    	templateUrl: 'app/views/editUserView.tpl.html',
        controller: 'EditUserController',
        resolve: {
        	user: user
        }
      }).
      when('/journals/update/:id', {
    	templateUrl: 'app/views/addOrUpdateJournalView.tpl.html',
        controller: 'AddOrUpdateJournalController',
        resolve: {
        	user: user
        }
      }).
      when('/journals/view/:id', {
    	templateUrl: 'app/views/browseJournalView.tpl.html',
        controller: 'BrowseJournalController',
        resolve: {
        	user: user
        }
      }).
      when('/releases/:id', {
    	templateUrl: 'app/views/viewJournalSourceView.tpl.html',
        controller: 'ViewJournalSourceController',
        resolve: {
        	user: user
        }
      }).
      when('/users/:userId/journals', {
            templateUrl: 'app/views/journalsView.tpl.html',
            controller: 'JournalsController',
            resolve: {
            	user: user
            }
          }).
      when('/addJournal', {
        templateUrl: 'app/views/addOrUpdateJournalView.tpl.html',
        controller: 'AddOrUpdateJournalController',
        resolve: {
        	user: user
        }
      }).
      when('/updateProfile', {
            templateUrl: 'app/views/updateProfileView.tpl.html',
            controller: 'UpdateProfileController',
            resolve: {
            	user: user
            }
          }).
      when('/login', {
        templateUrl: 'app/views/loginView.tpl.html',
        controller: 'LoginController'
      }).
      when('/register', {
        templateUrl: 'app/views/registerView.tpl.html',
        controller: 'RegisterController'
      }).
      otherwise({
        redirectTo: '/login'
      });
  });