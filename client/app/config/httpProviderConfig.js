 'use strict';
 
journalsApp.factory('httpInterceptor', function(configuration, identityConverter) {
	return {
	    request: function(config) {
	    	// Append api url to each request
	    	if(config.url.indexOf('api') !== -1 || config.url.indexOf('oauth') !== -1) {
	    		config.url = configuration.apiUrl + config.url;
	    	}
	    	
	    	// Make sure that there is no cyclic reference by using identity converter
	    	if(config.data) {
	    		config.data = identityConverter.toJson(config.data, "@id");
	    	}
	    	
	    	return config;
	    },

	    requestError: function(config) {
	      return config;
	    },

	    // Make sure that object references are in place using identity converter
	    response: function(res, lala) {
	      var obj = identityConverter.toObject(res, "@id");
	      
	      return obj;
	    },

	    responseError: function(res) {
	      return res;
	    }
	  }
}).config(function($httpProvider) {
	  $httpProvider.interceptors.push('httpInterceptor');
})