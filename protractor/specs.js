// spec.js
describe('Medical Journals App', function() {
  it('check authentication', function() {
    browser.get('http://localhost:9999/client');

    element(by.id('inputEmail')).sendKeys('demo@crossover.com');
    element(by.id('inputPassword')).sendKeys('crossover2016');

    element(by.css('[type="submit"]')).click();

    // Check if user is authenticated
    var authenticated = element(by.id('navbar')).evaluate('authenticated');

    expect(authenticated).toEqual(true);

    // Check if access token is stored
    var access_token = browser.executeScript("return window.localStorage.getItem('access_token');");

    expect(access_token).not.toEqual(undefined);
  });

  it('check if journals are loaded', function() {
    var totalElements = element(by.tagName('ng-view')).evaluate('unsubscribedJuornalsPage.totalElements');

    expect(totalElements).toBeGreaterThan(0);
  });
});