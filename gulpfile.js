var gulp = require('gulp'),
	gulpif = require('gulp-if'),
	useref = require('gulp-useref'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
	sass = require('gulp-sass'),
    del = require('del'),
    inject = require('gulp-inject'),
    bowerFiles = require('main-bower-files'),
    connect = require('gulp-connect'),
    ngAnnotate = require('gulp-ng-annotate'),
    templateCache = require('gulp-angular-templatecache');

gulp.task('buildTemplates', function(){
    gulp.src('client/**/*.tpl.html')
    .pipe(templateCache('templates.js', {standalone:true}))
    .pipe(notify({ message: 'Building templates complete.' }))
    .pipe(gulp.dest('client/app'));
});

gulp.task('index', ['build-css', 'buildTemplates'], function () {
  var target = gulp.src('client/index.html');
  var sources = gulp.src(['client/**/*.js', 'client/**/*.css'], {read: false});

  return target.pipe(inject(gulp.src(bowerFiles(), {read: false}), {name: 'bower', relative: true}))
  	.pipe(inject(sources, { relative: true }))
    .pipe(gulp.dest('client'));
});

gulp.task('build-css', function() {
  return gulp.src('client/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('client'))
    .pipe(notify({ message: 'Building css complete.' }))
    .pipe(connect.reload());;
});

gulp.task('scripts', ['index'], function () {
    return gulp.src('client/index.html')
        .pipe(useref())
        .pipe(gulpif('*.js', ngAnnotate()))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulp.dest('src/main/resources/static'))
        .pipe(notify({ message: 'Scripts task complete' }))
        .pipe(connect.reload());
});

gulp.task('copyFonts', function(){
    gulp.src(['bower_components/bootstrap/fonts/glyphicons-halflings-regular.*'])
   .pipe(notify({ message: 'Fonts copy complete.' }))
   .pipe(gulp.dest('src/main/resources/static/app/fonts'));
});

gulp.task('copy', ['copyFonts'], function(){
     gulp.src('client/**/*.css')
    .pipe(gulp.dest('src/main/resources/static'));
});

gulp.task('clean', function() {
    return del(['src/main/resources/static']);
});

gulp.task('connect', function() {
  connect.server({
    root: '',
    port: 9999,
    livereload: true
  });
});

gulp.task('html', function () {
  gulp.src('client/**/*.html')
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch('client/**/*.html', ['html', 'buildTemplates']);
  gulp.watch('client/**/*.js', ['scripts']);
  gulp.watch('client/**/*.scss', ['build-css']);
});

gulp.task('default', ['clean'], function() {
    return gulp.start('index', 'scripts', 'copy');
});

gulp.task('server', ['default', 'connect', 'watch']);

gulp.task('build', ['default']);